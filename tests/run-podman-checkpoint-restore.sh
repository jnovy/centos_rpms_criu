#!/bin/bash

set -eux

ls -la

if ! crun checkpoint --help; then
    echo "crun doesn't support checkpoint/restore"
    exit 0
fi

rpm -q crun podman criu

echo "Start container"
podman --log-level debug run -d quay.io/adrianreber/counter

echo "See which containers are running"
podman ps

echo "Connect to the container"
curl `podman inspect -l | jq -r '.[0].NetworkSettings.IPAddress'`:8088

CTR_ID=$(podman inspect -l --format '{{.Id}}')

echo "Checkpoint container"
podman --log-level debug container checkpoint -l

podman ps -a
echo "Restore container"
podman --log-level debug container restore "$CTR_ID"

podman ps -a
echo "Check if we can connect to the restored container"
curl `podman inspect -l | jq -r '.[0].NetworkSettings.IPAddress'`:8088

ls -la
